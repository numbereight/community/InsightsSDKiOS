//
//  NEXRecordingConfig.m
//  Insights
//
//  Created by Matthew Paletta on 2021-09-06.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#import "NEXRecordingConfig.h"

#define NE_USE_IOS_TOPICS
#import <NumberEightCompiled/NumberEightCompiled.h>
#undef NE_USE_IOS_TOPICS

#include <stdlib.h>

#include <limits>

// Encoding Keys
static NSString* const LOG_TAG = @"NEXRecordingConfig";
static NSString* const kDeviceIdKey = @"deviceId";
static NSString* const kUploadWithWifiOnlyKey = @"uploadWithWifiOnly";
static NSString* const kInitialUploadDelayKey = @"initialUploadDelay";
static NSString* const kUploadIntervalKey = @"uploadInterval";
static NSString* const kMostProbableOnlyKey = @"mostProbableOnly";
static NSString* const kTopicsKey = @"topics";
static NSString* const kFiltersKey = @"filters";

// 3 seconds
static int kNEDefaultInitialUploadDelay = 3;

// 3 minutes
static int kNEDefaultUploadInterval = 5 * 60;

@implementation NEXRecordingConfig

@synthesize deviceId;
@synthesize uploadWithWifiOnly;
@synthesize initialUploadDelay;
@synthesize uploadInterval;
@synthesize mostProbableOnly;
@synthesize topics;
@synthesize filters;

- (instancetype)initWithDeviceId:(NSString*)initialDeviceID
              uploadWithWifiOnly:(BOOL)initialUploadWithWifiOnly
              initialUploadDelay:(NSTimeInterval)theInitialUploadDelay
                  uploadInterval:(NSTimeInterval)theUploadInterval
                mostProbableOnly:(BOOL)initialMostProbableOnly
                          topics:(NSSet<NSString*>*)initialTopics
                         filters:(NSDictionary<NSString*, NSString*>*)initialFilters {
    self = [super init];
    if (self) {
        self.deviceId = initialDeviceID;
        self.uploadWithWifiOnly = initialUploadWithWifiOnly;
        self.initialUploadDelay = theInitialUploadDelay;
        self.uploadInterval = theUploadInterval;
        self.mostProbableOnly = initialMostProbableOnly;

        self.topics = [[NSMutableSet alloc] initWithSet:initialTopics];
        self.filters = [[NSMutableDictionary alloc] initWithDictionary:initialFilters];
    }

    return self;
}

+ (instancetype _Nullable)fromJSONString:(NSString* _Nullable)string {
    // JSON String
    NSData* strData = [string dataUsingEncoding:NSUTF8StringEncoding];
    if (!strData) {
        [NELog msg:LOG_TAG error:@"Got nil data while converting JSON to RecordingConfig"];
        return nil;
    }

    NSError* error = nil;
    NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:strData options:0 error:&error];
    if (error) {
        [NELog msg:LOG_TAG
               error:@"Failed to parse string to JSON Object for RecordingConfig:fromJSONString."
            metadata:@{ @"string" : string, @"error" : error.debugDescription }];
        return nil;
    }

    return [NEXRecordingConfig fromDictionary:dict];
}

- (instancetype _Nonnull)init {
    self = [super init];
    if (self) {
        NSDictionary<NSString*, NSString*>* defaultFilters =
            [[NSMutableDictionary alloc] initWithDictionary:@{
                kNETopicActivity : @"movavg:20s|uniq",
                kNETopicBatteryLevel : @"uniq",
                kNETopicConnectionPerformance : @"hyst:3s|uniq",
                kNETopicDevicePosition : @"movavg:10s|uniq",
                kNETopicIndoorOutdoor : @"movavg:20s|uniq",
                kNETopicHeadphonesWired : @"hyst:10s|uniq",
                kNETopicHeadphonesBluetooth : @"hyst:10s|uniq",
                kNETopicLatencyUpload : @"",  // Filter set by SSP core-side
                kNETopicGeo : @"uniq",
                kNETopicReachability : @"hyst:20s|uniq",
                kNETopicSituation : @"hyst:20s|uniq",
                kNETopicPlace : @"hyst:10s",
                kNETopicTime : @"uniq",
                kNETopicWeather : @"uniq",
                kNETopicVolumeLevelGranularCategory : @"uniq"
            }];
        NSSet<NSString*>* defaultTopics =
            [[NSMutableSet alloc] initWithArray:defaultFilters.allKeys];

        self.deviceId = nil;
        self.uploadWithWifiOnly = false;
        self.initialUploadDelay = kNEDefaultInitialUploadDelay;
        self.uploadInterval = kNEDefaultUploadInterval;
        self.mostProbableOnly = true;

        self.topics = [[NSMutableSet alloc] initWithSet:defaultTopics];
        self.filters = [[NSMutableDictionary alloc] initWithDictionary:defaultFilters];
    }

    return self;
}

+ (NEXRecordingConfig*)defaultConfig {
    return [[NEXRecordingConfig alloc] init];
}

- (void)encodeWithCoder:(NSCoder*)encoder {
    auto dict = [self asDictionary];

    for (NSString* key in dict.allKeys) {
        [encoder encodeObject:dict[key] forKey:key];
    }
}

- (instancetype)initWithCoder:(NSCoder* _Nullable)coder {
    self = [super init];
    if (self) {
        self.deviceId = [coder decodeObjectOfClass:[NSString class] forKey:kDeviceIdKey];
        self.uploadWithWifiOnly = [[coder decodeObjectOfClass:[NSNumber class]
                                                       forKey:kUploadWithWifiOnlyKey] boolValue];
        self.initialUploadDelay = [[coder decodeObjectOfClass:[NSNumber class]
                                                       forKey:kInitialUploadDelayKey] doubleValue];
        self.uploadInterval = [[coder decodeObjectOfClass:[NSNumber class]
                                                   forKey:kUploadIntervalKey] doubleValue];
        self.mostProbableOnly = [[coder decodeObjectOfClass:[NSNumber class]
                                                     forKey:kMostProbableOnlyKey] boolValue];

        if (@available(iOS 14.0, *)) {
            NSArray* decodedTopics = [coder decodeArrayOfObjectsOfClass:[NSString class]
                                                                 forKey:kTopicsKey];
            if (decodedTopics != nil) {
                self.topics = [[NSSet alloc] initWithArray:decodedTopics];
            } else {
                self.topics = [NSSet new];
            }
        } else {
            self.topics =
                [coder decodeObjectOfClasses:[[NSSet alloc] initWithObjects:[NSString class], nil]
                                      forKey:kTopicsKey];
        }

        if (@available(iOS 14.0, *)) {
            NSDictionary* decodedFilters = [coder decodeDictionaryWithKeysOfClass:[NSString class]
                                                                   objectsOfClass:[NSString class]
                                                                           forKey:kFiltersKey];
            if (decodedFilters != nil) {
                self.filters = (NSDictionary* _Nonnull)decodedFilters;
            } else {
                self.filters = [NSDictionary new];
            }
        } else {
            self.filters =
                [coder decodeObjectOfClasses:[[NSSet alloc] initWithObjects:[NSDictionary class],
                                                                            [NSString class], nil]
                                      forKey:kFiltersKey];
        }
    }
    return self;
}

+ (BOOL)supportsSecureCoding {
    return YES;
}

+ (NEXRecordingConfig* _Nullable)fromDictionary:(NSDictionary* _Nullable)dict {
    if (!dict) {
        return nil;
    }

    auto unableToParseKeyLog = [](NSString* key) -> NEXRecordingConfig* {
        [NELog msg:LOG_TAG
               error:@"Unable to parse key from JSON object."
            metadata:@{ @"key" : key }];
        return nil;
    };

    NSString* deviceIdParsed = dict[kDeviceIdKey];
    if (deviceIdParsed == nil) {
        return unableToParseKeyLog(kDeviceIdKey);
    }
    NSNumber* uploadWithWifiOnlyParsed = dict[kUploadWithWifiOnlyKey];
    if (uploadWithWifiOnlyParsed == nil) {
        return unableToParseKeyLog(kUploadWithWifiOnlyKey);
    }
    NSNumber* initialUploadDelayParsed = dict[kInitialUploadDelayKey];
    if (initialUploadDelayParsed == nil) {
        return unableToParseKeyLog(kInitialUploadDelayKey);
    }
    NSNumber* uploadIntervalParsed = dict[kUploadIntervalKey];
    if (uploadIntervalParsed == nil) {
        return unableToParseKeyLog(kUploadIntervalKey);
    }
    NSNumber* mostProbableOnlyParsed = dict[kMostProbableOnlyKey];
    if (mostProbableOnlyParsed == nil) {
        return unableToParseKeyLog(kMostProbableOnlyKey);
    }
    NSSet<NSString*>* topicsParsed = dict[kTopicsKey];
    if (topicsParsed == nil) {
        return unableToParseKeyLog(kTopicsKey);
    }
    NSDictionary<NSString*, NSString*>* filtersParsed = dict[kFiltersKey];
    if (filtersParsed == nil) {
        return unableToParseKeyLog(kFiltersKey);
    }

    return [[NEXRecordingConfig alloc] initWithDeviceId:deviceIdParsed
                                     uploadWithWifiOnly:uploadWithWifiOnlyParsed.boolValue
                                     initialUploadDelay:initialUploadDelayParsed.doubleValue
                                         uploadInterval:uploadIntervalParsed.doubleValue
                                       mostProbableOnly:mostProbableOnlyParsed.boolValue
                                                 topics:topicsParsed
                                                filters:filtersParsed];
}

- (NSDictionary*)asDictionary {
    NSMutableDictionary* output = [NSMutableDictionary new];
    [output setValue:self.deviceId forKey:kDeviceIdKey];
    [output setValue:[[NSNumber alloc] initWithBool:self.uploadWithWifiOnly]
              forKey:kUploadWithWifiOnlyKey];
    [output setValue:[[NSNumber alloc] initWithDouble:self.initialUploadDelay]
              forKey:kInitialUploadDelayKey];
    [output setValue:[[NSNumber alloc] initWithDouble:self.uploadInterval]
              forKey:kUploadIntervalKey];
    [output setValue:[[NSNumber alloc] initWithBool:self.mostProbableOnly]
              forKey:kMostProbableOnlyKey];
    [output setValue:self.topics.allObjects forKey:kTopicsKey];
    [output setValue:self.filters forKey:kFiltersKey];

    return output;
}

- (BOOL)isEqual:(id)object {
    if (object == nil || ![object isKindOfClass:[NEXRecordingConfig class]]) {
        return NO;
    }

    NEXRecordingConfig* other = (NEXRecordingConfig*)object;
    return [self.deviceId isEqualToString:other.deviceId] &&
           self.uploadWithWifiOnly == other.uploadWithWifiOnly &&
           std::abs(self.initialUploadDelay - other.initialUploadDelay) <= 0.00001 &&
           std::abs(self.uploadInterval - other.uploadInterval) <= 0.00001 &&
           self.mostProbableOnly == other.mostProbableOnly &&
           [self.topics isEqualToSet:other.topics] &&
           [self.filters isEqualToDictionary:other.filters];
}

@end
