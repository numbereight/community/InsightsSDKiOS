//
//  NEXIABAudience.m
//  Insights
//
//  Created by Matthew Paletta on 2021-09-06.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#import "NEXIABAudience.h"

#import <NumberEightCompiled/NELog.h>

static NSString* const LOG_TAG = @"NEXIABAudience";
static NSString* const kIdKey = @"id";
static NSString* const kExtensionsKey = @"extensions";

@interface NEXIABAudience ()

@property(atomic, readwrite, strong) NSString* _Nonnull ID;
@property(atomic, readwrite, strong) NSArray<NSString*>* _Nonnull extensions;

@end

@implementation NEXIABAudience

@synthesize ID;
@synthesize extensions;

- (instancetype _Nonnull)initWithID:(NSString* _Nonnull)initialID {
    self = [super init];
    if (self) {
        self.ID = initialID;
        self.extensions = [NSArray new];
    }

    return self;
}

- (instancetype _Nonnull)initWithID:(NSString* _Nonnull)initialID
                         extensions:(NSArray<NSString*>* _Nonnull)initialExtensions {
    self = [super init];
    if (self) {
        self.ID = initialID;
        self.extensions = initialExtensions;
    }

    return self;
}

- (NSString* _Nonnull)extendedId {
    if (self.extensions.count == 0) {
        return self.ID;
    } else {
        return [NSString
            stringWithFormat:@"%@|%@", self.ID,
                             [[self.extensions sortedArrayUsingSelector:@selector(compare:)]
                                 componentsJoinedByString:@"|"]];
    }
}

- (NSString* _Nonnull)description {
    return self.ID;
}

- (void)encodeWithCoder:(NSCoder*)encoder {
    [encoder encodeObject:self.ID forKey:kIdKey];
    [encoder encodeObject:self.extensions forKey:kExtensionsKey];
}

- (NSDictionary*)asDictionary {
    NSMutableDictionary* output = [NSMutableDictionary new];
    [output setValue:self.ID forKey:kIdKey];
    [output setValue:self.extensions forKey:kExtensionsKey];
    return output;
}

+ (instancetype _Nullable)fromDictionary:(NSDictionary* _Nullable)dict {
    if (!dict) {
        return nil;
    }

    auto unableToParseKeyLog = [](NSString* key) -> NEXIABAudience* {
        [NELog msg:LOG_TAG
               error:@"Unable to parse key from JSON object."
            metadata:@{ @"key" : key }];
        return nil;
    };

    NSString* idParsed = dict[kIdKey];
    if (!idParsed) {
        return unableToParseKeyLog(kIdKey);
    }
    NSArray<NSString*>* extensionsParsed = dict[kExtensionsKey];
    if (!extensionsParsed) {
        return unableToParseKeyLog(kExtensionsKey);
    }

    return [[NEXIABAudience alloc] initWithID:idParsed extensions:extensionsParsed];
}

- (instancetype)initWithCoder:(NSCoder* _Nullable)coder {
    self = [super init];
    if (self) {
        self.ID = [coder decodeObjectOfClass:[NSString class] forKey:kIdKey];

        if (@available(iOS 14.0, *)) {
            NSArray* decodedExtensions = [coder decodeArrayOfObjectsOfClass:[NSString class]
                                                                     forKey:kExtensionsKey];
            if (decodedExtensions != nil) {
                self.extensions = (NSArray* _Nonnull)decodedExtensions;
            } else {
                self.extensions = [NSArray new];
            }
        } else {
            self.extensions =
                [coder decodeObjectOfClasses:[[NSSet alloc] initWithObjects:[NSArray class],
                                                                            [NSString class], nil]
                                      forKey:kExtensionsKey];
        }
    }
    return self;
}

+ (BOOL)supportsSecureCoding {
    return YES;
}

- (BOOL)isEqual:(id)object {
    if (object == nil || ![object isKindOfClass:[NEXIABAudience class]]) {
        return NO;
    }

    NEXIABAudience* other = (NEXIABAudience*)object;
    return self.ID == other.ID && [self.extensions isEqualToArray:other.extensions];
}

- (NSUInteger)hash {
    NSUInteger hash = 17;
    hash = hash * 31 + [[NEXIABAudience class] hash];
    hash = hash * 31 + [self.ID hash];
    hash = hash * 31 + [self.extensions hash];
    return hash;
}

@end
