## Build instructions
This project is built with Xcode.

[![pipeline status](https://gitlab.com/numbereight/community/InsightsSDKiOS/badges/master/pipeline.svg)](https://gitlab.com/numbereight/community/InsightsSDKiOS/-/commits/master)

## Maintainers

* [Matthew Paletta](matt@numbereight.me)
* [Chris Watts](chris@numbereight.me)
